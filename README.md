PCIe Debug Tool
===============

Usage:

Run the following command as root:

    $ pciedbg.py
    
In case of PCIe transmission errors with the FLX-712 cards, you should see an output like this:

    2019-07-23 16:33:54 04:00.0 CorrectableError
    2019-07-23 16:34:02 04:00.0 CorrectableError
    2019-07-23 16:34:07 04:00.0 CorrectableError
    2019-07-23 16:34:08 04:00.0 CorrectableError
    2019-07-23 16:34:09 04:00.0 CorrectableError

No output means no errors.

Additional statistics will be printed when exiting the program with Ctrl+C:

    # TOTAL
    #  04:00.0: Correctable Errors: 1 (5.4/min)     Non Fatal Errors: 0 (0.0/min)   Fatal Errors: 0 (0.0/min)       Unsupported Requests: 0 (0.0/min)
    #  05:00.0: Correctable Errors: 0 (0.0/min)     Non Fatal Errors: 0 (0.0/min)   Fatal Errors: 0 (0.0/min)       Unsupported Requests: 0 (0.0/min)
    #  08:00.0: Correctable Errors: 0 (0.0/min)     Non Fatal Errors: 0 (0.0/min)   Fatal Errors: 0 (0.0/min)       Unsupported Requests: 0 (0.0/min)
    #  09:00.0: Correctable Errors: 0 (0.0/min)     Non Fatal Errors: 0 (0.0/min)   Fatal Errors: 0 (0.0/min)       Unsupported Requests: 0 (0.0/min)


Error Types
-----------

See here for an explanation of PCIe error flags: http://billauer.co.il/blog/2011/07/pcie-tlp-dllp-retransmit-data-link-layer-error/

The most common error type observed with FLX cards is "Correctable Error", which implies a TLP retransmit.