#!/usr/bin/env python

import subprocess
import time
import datetime
import argparse

class ErrorCounter(object):
    def __init__(self):
        self.correctable_errors = 0
        self.non_fatal_errors = 0
        self.fatal_errors = 0
        self.unsupported_requests = 0
        self.reset_timer()

    def reset_timer(self):
        """Reset timer to compute rates"""
        self.start_time = datetime.datetime.now()

    def update(self, flags):
        """Update internal counters according to the given PCIe flags"""
        if flags.correctable_error: self.correctable_errors += 1
        if flags.non_fatal_error: self.non_fatal_errors += 1
        if flags.fatal_error: self.fatal_errors += 1
        if flags.unsupported_request: self.unsupported_requests += 1

    def __str__(self):
        now = datetime.datetime.now()
        minutes = (now - self.start_time).total_seconds()/60.0

        s = list()
        s.append("Correctable Errors: {:d} ({:.1f}/min)".format(self.correctable_errors, self.correctable_errors/minutes))
        s.append("Non Fatal Errors: {:d} ({:.1f}/min)".format(self.non_fatal_errors, self.non_fatal_errors/minutes))
        s.append("Fatal Errors: {:d} ({:.1f}/min)".format(self.fatal_errors, self.fatal_errors/minutes))
        s.append("Unsupported Requests: {:d} ({:.1f}/min)".format(self.unsupported_requests, self.unsupported_requests/minutes))
        return " \t".join(s)
        

class PCIeFlags(object):
    def __init__(self, flags):
        self.flags = flags

    @property
    def correctable_error(self):
        return self.flags & 0x1

    @property
    def non_fatal_error(self):
        return self.flags & 0x2

    @property
    def fatal_error(self):
        return self.flags & 0x4
    
    @property
    def unsupported_request(self):
        return self.flags & 0x8
    
    def __str_verbose__(self):
        s = ['Correctable', 'NonFatal', 'Fatal', 'UnsupReq']
        s[0] += '+' if self.correctable_error else '-'
        s[1] += '+' if self.non_fatal_error else '-'
        s[2] += '+' if self.fatal_error else '-'
        s[3] += '+' if self.unsupported_request else '-'
        return ' '.join(s)

    def __str__(self):
        s = list()
        if self.correctable_error: s.append('CorrectableError')
        if self.non_fatal_error: s.append('NonFatalError' )
        if self.fatal_error: s.append('FatalError' )
        if self.unsupported_request: s.append('UnsupportedRequest')
        return ' '.join(s)

def scan_flx_devices():
    '''Returns the bus addresses of all FLX cards in the system.'''
    lspci = subprocess.check_output('lspci').splitlines()
    lspci = [ x for x in lspci if "Xilinx" in x ]
    return [ x.split()[0] for x in lspci ]


def read_flags(address):
    '''Read error flags from the device indicated by the PCIe bus address.'''
    flags = subprocess.check_output(['setpci', '-s', address, 'CAP_EXP+0xa.w'])
    flags = int(flags, base=16)
    return PCIeFlags(flags) 


def reset_flags(address):
    '''Reset error flags for the given PCIe device'''
    subprocess.check_call(['setpci', '-s', address, 'CAP_EXP+0xa.w=0x000f'])

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="Test PCIe cards for error flags")
    parser.add_argument("devices", metavar="DEV", type=str, nargs="*", help="PCIe addresses of devices to scan for errors. If no addresses are given, all FLX cards of the system are used by default.")
    args = parser.parse_args()

    if len(args.devices) > 0:
        devices = args.devices
    else:
        devices = scan_flx_devices()
    counters = { d:ErrorCounter() for d in devices }

    try:
        while True:
            for d in devices:
                reset_flags(d)
            time.sleep(1)
            now = datetime.datetime.now()
            for d in devices:
                flags = read_flags(d)
                if flags.flags == 0:
                    continue
                counters[d].update(flags)
                print("{:%Y-%m-%d %H:%M:%S} {} {}".format(now, d, flags))
    except:
        print("\n\n# TOTAL")
        for d in devices:
            print("#  {}: {}".format(d, counters[d]))
